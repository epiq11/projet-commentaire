<?php
  include_once './functions/contact-functions.php';
  include_once './functions/data/connecteur.php';
  include_once './functions/data/contact.php';
  include_once './includes/parts/header.php';
  include_once './includes/search-header.php';

?>

<section>
<h1 class="title is-1" >Contacter nous!</h1>
    <div class="container is-fluid">
        <form action="/contact.php" method="POST" >
            <input placeholder="nom" type="text" name="nom" value="<?php form_values("nom"); ?>"><br/>
            <input placeholder="prenom" type="text" name="prenom" value="<?php form_values("prenom"); ?>"><br/>
            <input placeholder="courriel" type="email" name="courriel" value="<?php form_values("courriel"); ?>"><br/>
            <textarea name="contact" id="contact" placeholder="Votre message" cols="30" rows="10"><?php form_values("contact"); ?></textarea><br/>
            <input type="submit" value="Soumettre votre demande">
        </form>

    </div>
</section>

<?php
  include_once './includes/parts/comment-list.php';
    if (isset($errors) && !empty($errors)) {
?>

<section>
  <h1 class="subtitle is-2">Erreurs</h1>
  <div>
    <?php
          foreach($errors as $error){
            echo "<div class='error'>" . $error . "</div>";
          }
    ?>
  </div>
</section>

<?php
}
  include_once './includes/parts/footer.php';
?>
