<section class="hero">
  
  <div class="hero-body">
    <div class="container is-fluid">
      <form action="/" method="POST">
        <input class="input is-medium" type="text" name="prenom" placeholder="Prénom" /><br />
        <input class="input is-medium" type="text" name="nom" placeholder="Nom" /><br />
        <textarea class="input" name="comment" id="comment" placeholder="Entrez votre commentaire ici!" cols="30"
          rows="10"></textarea>
        <br />
        <div class="control">
          <input type="submit" Value="Commenter" class="button is-primary">
        </div>
      </form>
    </div>
  </div>

   <div id="presentation" class="container is-fluid">
      <ul>
        <li>Allo</li>
        <li>Bonjour</li>
        <li>Salut</li>
        <li>Allo</li>
        <li>Bonjour</li>
        <li>Salut</li>
      </ul>
   </div>

  <div class="operations">
    <ul>
      <li>1-plt</li>
      <li>2-plt</li>
      <li>3-plt</li>
    </ul>
    <div>
      <p>lorem</p>
    </div>
  </div>

</section>
<hr>