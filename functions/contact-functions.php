<?php
$errors = null;

function form_values($nom){
    echo (isset($_POST[$nom]) ? $_POST[$nom] : "");
}

function validate_contact(){
        $errors = [];
        $errors["nom"] = (empty($_POST["nom"])) ? "Le nom ne peut pas être vide." : "";
        $errors["prenom"] = (empty($_POST["prenom"])) ? "Le prénom ne peut pas être vide." : "";
        $errors["courriel"] = (empty(validate_email($_POST["courriel"]))) ? "Le courriel ne peut pas être vide et doit être formatez correctement." : "";
        $errors["contact"] = (strlen($_POST["contact"])) < 3 ? "Le message doit avoir plus de 3 lettres." : "";

        if (count($errors) >= 1 ) {
            return $errors;
        }
}

function validate_email($courriel){
    return filter_var($courriel, FILTER_VALIDATE_EMAIL);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $form = validate_contact();
    if (count($form) > 1) {
        $errors = $form;
    }

    return $errors;
}

?>