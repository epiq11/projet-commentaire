<?php

function all_comments(){
    $conn = connect();
    try{
        $requete = $conn->query('SELECT * from comments');
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function comment_by_id($id){
    $conn = connect();
    try{
        $requete = $conn->prepare("SELECT * FROM comments Where Id = :id"); //:id sert de placehold pour démarre la requete et remplacer par autre chose
        $requete->execute([":id"=>$id]);
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

